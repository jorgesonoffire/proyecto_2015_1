class cliente():
    def __init__(self,nombre,rut,compras):
        self.nombre=nombre
        self.rut=rut
        self.compras=compras
    def imprimir(self):
        print(self.nombre,'    |     ',self.rut)
        if len(self.compras)>0:
            print('compras realizadas:')
            for x in self.compras:
                print(x)
        else:
            print('no hay compras realizadas')

class producto():
    def __init__(self,nombre,precio,cantidad):
        self.nombre=nombre
        self.precio=precio
        self.cantidad=cantidad
    def imprimir(self):
        print(self.nombre,'     |     ',self.precio,'      |     ',self.cantidad,'    |    ','    -    ','    |    ','    -    ')
    
class aseo(producto):
    def __init__(self,nombre,precio,cantidad,toxicidad):
        super(aseo,self).__init__(nombre,precio,cantidad)
        self.toxicidad =toxicidad
    def imprimir(self):
        print(self.nombre,'    |     ',self.precio,'     |     ',self.cantidad,'       |    ','      -      ','    |    ',self.toxicidad)
    
class perecible(producto):
    def __init__(self,nombre,precio,cantidad,fechavencimiento):
        super(perecible,self).__init__(nombre,precio,cantidad)
        self.fechavencimiento=fechavencimiento
    def imprimir(self):
        print(self.nombre,'    |     ',self.precio,'     |     ',self.cantidad,'     |     ',self.fechavencimiento,'    |    ','       -   ')
  
  
      